#-----------------------------------
#------------- PART 3 --------------
#-----------------------------------

import pandas as pd

# First, we need to import the .csv database into Python
Coffeebar = pd.read_csv\
    ('/Users/jibebo/Google Drive/M2 FiRE/Python/Project/ExamTSM2018/Data/Coffeebar_2013-2017.csv',
     names=['Time', 'Customer', 'Drinks', 'Food'], header=0,
     delimiter=';')

# Implementing some parameters

NbReturning = 1000  # Number of returning customers
NbHipsters = 1000/3  # Number of hipsters
NbRegular = NbReturning - NbHipsters  # Number of regular customers

probReturning = 0.2  # Probability to have a returning customer
probOnetime = 0.8  # Probability to have a one time customer


# Pricing of the items

food_prices = {'nothing': 0, 'sandwich': 5, 'cookie': 2, 'muffin': 3, 'pie': 3}
drinks_prices = {'milkshake': 5, 'frappucino': 4, 'water': 2, 'coffee': 3, 'tea': 3, 'soda': 3,}