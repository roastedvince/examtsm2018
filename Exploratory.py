# -----------------------------------
# ------------- PART 1 --------------
# -----------------------------------

import pandas as pd
import os
import matplotlib.pyplot as plt
import matplotlib
matplotlib.style.use('ggplot')

# First, we need to import the .csv database into Python
Coffeebar = pd.read_csv \
    ('/Users/jibebo/Google Drive/M2 FiRE/Python/Project/ExamTSM2018/Data/Coffeebar_2013-2017.csv',
     names=['Time', 'Customer', 'Drinks', 'Food'], header=0,
     delimiter=';')

# What food and drinks are sold by the coffee bar?

Foods = Coffeebar['Food'].unique()  # List of food
print('List of available food:' + str(Foods))

Drinks = Coffeebar['Drinks'].unique() # List of drinks
print('List of available drinks:' + str(Drinks))

# 2 How many unique customers did the bar have?

NbCust = len(Coffeebar['Customer'].unique())  # Number of customers
print('Number of unique customers: ' + str(NbCust))

# 3 Bar plot of the total amount of sold foods

food_sold = [len(Coffeebar[Coffeebar['Food'] == typefood]) for typefood in Foods]  # Compute the amount of food sold for
# each type of food
print(food_sold)

plt.bar(Foods, food_sold)  # Plot
plt.grid(True)
plt.savefig(os.path.abspath('./Output/food.png'))
plt.show()

# 4 Bar plot of the total amount of sold drinks

drinks_sold = [len(Coffeebar[Coffeebar['Drinks'] == typedrink]) for typedrink in Drinks]  # Compute the amount of
# drinks sold for each type of food
print(drinks_sold)

plt.bar(Drinks, drinks_sold) # Plot
plt.grid(True)
plt.savefig(os.path.abspath('./Output/drinks.png'))
plt.show()


# 5 Determine the probabilities

# First we need to extract the time which is between the 11th and the 16th characters

def time(i):
    print(Coffeebar['Time'][i][11:16])
    return Coffeebar['Time'][i][11:16]


#   Compute the probability to choose a certain food at a given time

def food_proba(hours, typefood):
    consperday = len(Coffeebar[Coffeebar['Hours'] == hours])  # Daily consumption
    food_temp = Coffeebar[(Coffeebar['Hours'] == hours) & (Coffeebar['Food'] == typefood)]
    print(hours, typefood, (len(food_temp) / consperday) * 100)
    return (len(food_temp) / consperday) * 100


#   Probability to choose a certain drink at a given time

def drink_proba(hours, typedrink):
    consperday = len(Coffeebar[Coffeebar['Hours'] == hours])  # Daily consumption
    drink_temp = Coffeebar[(Coffeebar['Hours'] == hours) & (Coffeebar['Drinks'] == typedrink)]
    print(hours, typedrink, (len(drink_temp) / consperday) * 100)
    return (len(drink_temp) / consperday) * 100