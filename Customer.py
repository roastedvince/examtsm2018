
#-----------------------------------
#------------- PART 2 --------------
#-----------------------------------

from random import randint
import pandas as pd

# First, we need to import the .csv database into Python
Coffeebar = pd.read_csv\
    ('/Users/jibebo/Google Drive/M2 FiRE/Python/Project/ExamTSM2018/Data/Coffeebar_2013-2017.csv',
     names=['Time', 'Customer', 'Drinks', 'Food'], header=0,
     delimiter=';')

# Name of the columns in the final output
cols = ['Time', 'customerID', 'Drinks', 'Drinks price', 'Food', 'Food price', 'Amount paid']


# First step : Define the customers with their characteristics

class Customer(object):
    def __init__(self, customerID, budget):
        self.customerID = customerID
        self.budget = budget


# Show the budget of each customer

    def __repr__(self):
        return '{} has a budget of {} euros'.format(self.customerID, self.budget)


# Define the new budget after the purchase

    def pay(self, drink_price, food_price):
        if self.budget - drink_price - food_price < 0:
            print('Dishonoured payment')
        else:
            self.budget -= drink_price + food_price


# Then we have to define the one time customers who have a budget of 100 euros and pay a tip between 1 and 10 euros

class Onetime(Customer):
    def __init__(self, customerID):
        Customer.__init__(self, customerID)
        self.customerID = customerID
        self.budget = 100


# Define the tips of the one time customers

    def def_tip(self):
        tip = randint(1, 10)
        self.budget -= tip
        print('{} paid a tip of {} euros'.format(self.id, tip))
        return tip


# Define the tripadvisor customers

class Tripadvisor(Onetime):
    def __init__(self, customerID):
        Onetime.__init__(self, customerID)
        self.customerID = customerID


# Define the regular customers who have a budget of 250 euros

class Regular(Customer):
    def __init__(self, customerID):
        Customer.__init__(self, customerID)
        self.customerID = customerID
        self.budget = 250
        self.history = pd.DataFrame(data=None, index=None, columns=cols)

# Payment with the history for the returning customers

    def payment_history(self, time, drink, drink_price, food, food_price):
        if self.budget - drink_price - food_price < 0:
            print('Dishonoured payment')
        else:
            self.budget -= drink_price + food_price
            payment_temp = pd.DataFrame([[time, drink, drink_price, food, food_price, drink_price+food_price]], columns=cols)
            self.history = pd.DataFrame.append(self.history, payment_temp, ignore_index=True)


# Define the hipster class with a budget of 500 euros

class Hipster(Returning):
    def __init__(self, customerID):
        Returning.__init__(self, identity)
        self.customerID = customerID
        self.budget = 500